# desy-ftag-summie2022

Flavour tagging project during DESY summer student programme 2022

## Getting started

This gitlab project can be used to save the progress for the Dips flavour tagging algorithm study.

Currently, two notebooks are provided which can be used with Jupyter.

- prepare_samples.ipynb: provides information about the input datasets used for the training and explains the steps in the preprocessing of the samples and how the hybrid sample got created (already done using the umami software)

- train_Dips.ipynb: the real workhorse, this notebook is used to define the Dips neural network architecture and train it using a hybrid sample which is provided at DESY NAF.


## How to use git

### add & commit
You can propose changes (add it to the Index) using
```
git add <filename>
git add *
```
This is the first step in the basic git workflow. To actually commit these changes use
```
git commit -m "Commit message"
```
Now the file is committed to the HEAD, but not in your remote repository yet.
    
### pushing changes
Your changes are now in the HEAD of your local working copy. To send those changes to your remote repository, execute 
```
git push origin master
```
Change master to whatever branch you want to push your changes to.

### update

To update your local repository to the newest commit, execute 
```
git pull
```
in your working directory to fetch and merge remote changes.